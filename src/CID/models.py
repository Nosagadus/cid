from django.db import models

# Create your models here.
class Articulo(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField("Nombre Articulo", max_length=50)
    desc = models.TextField("Descripcion", null=True)
    costo = models.FloatField("Costo")
    off = models.IntegerField("Descuento",default=1)
    precio =models.FloatField("Precio")
    def __unicode__(self):
        return self.nombre

class Region(models.Model):
    id = models.AutoField(primary_key=True)
    nombre =models.CharField("Nombre Region", max_length=30)
    def __unicode__(self):
        return self.nombre

class Provincia(models.Model):
    id = models.AutoField(primary_key=True)
    region = models.ForeignKey(Region)
    nombre = models.CharField("Nombre Povincia", max_length=30)
    def __unicode__(self):
        return self.nombre

class Comuna(models.Model):
    id = models.AutoField(primary_key=True)
    provincia = models.ForeignKey(Provincia)
    nombre = models.CharField("Nombre Comuna", max_length=30)
    def __unicode__(self):
        return self.nombre

    
class Resinto(models.Model):
    id = models.AutoField(primary_key=True)
    comuna = models.ForeignKey(Comuna)
    nombre = models.CharField("Nombre Resinto", max_length=60)
    direccion = models.TextField(null=True)
    telefonos = models.CharField("Telefonos", max_length=30, null=True)
    observaciones = models.TextField(null=True)
    tipo_estab = (
        ('0','Otro'),
        ('1','Liceo'),
        ('2','Escuela'),
        ('3','Colegio'),
    )
    tipo = models.CharField(max_length=1, choices=tipo_estab)
    def __unicode__(self):
        return self.nombre

class Exposicion(models.Model):
    id = models.AutoField(primary_key=True)
    resinto = models.ForeignKey(Resinto)
    fecha = models.DateField(auto_now_add=True)
    def __unicode__(self):
        return self.fecha
    

class Prefijo(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField("Nombre Prefijo", max_length=10)
    def __unicode__(self):
        return self.nombre
    
class Cliente(models.Model):
    id = models.AutoField(primary_key=True)
    num = models.CharField("RUT", max_length=10)
    nombres = models.CharField("Nombre Cliente", max_length=30)
    telefonos = models.CharField("Telefonos", max_length=30, null=True)
    direccion = models.TextField("Direccion", null=True)
    email = models.EmailField("Email", null=True)
    prefijos = (
        ('0',''),
        ('1', 'Sr/a.'),
        ('2', 'Tio/a'),
        ('3', 'Prf/a.'),
        ('4', 'Dr/a.'),
    )
    calificaciones = (
        ('0','Peligroso'),
        ('1','Regular'),
        ('2','Neutro'),
        ('3','Bueno'),
        ('4','De Confianza'),
        )
    #prefijo = models.CharField(max_length=4, choices=prefijos, default=0)
    prefijo = models.ForeignKey(Prefijo)
    tipo = models.CharField(max_length=1, choices=calificaciones, default=2)
    habilitado = models.NullBooleanField(default=False)
    afiliado = models.DateField(auto_now_add=True)
    recintos = models.ManyToManyField(Resinto)
    def __unicode__(self):
        return self.nombres

class Deuda(models.Model):
    id = models.AutoField(primary_key=True)
    cliente = models.ForeignKey(Cliente)
    cuotas_pendientes = models.IntegerField()
    fecha_inicial = models.DateField(auto_now_add=True)
    prorroga = models.FloatField(default=1.0)
    total = models.FloatField("Total Adeudado")
    estado_deuda = (
        ('0','Pendiente'),
        ('1','Pagada'),
        ('2','Devolucion'),
        ('3','Inpaga'),
    )
    estado = models.CharField(max_length=1, choices=estado_deuda, default=0)
    articulos = models.ManyToManyField(Articulo)
    def __unicode__(self):
        return self.cliente

class Venta(models.Model):
    id = models.AutoField(primary_key=True)
    articulo = models.ForeignKey(Articulo)
    deuda = models.ForeignKey(Deuda)
    obs = models.TextField("Observaciones", null=True)
    precio = models.FloatField()

class Pago(models.Model):
    id = models.AutoField(primary_key=True)
    deudas = models.ForeignKey(Deuda)
    observaciones = models.TextField(null=True)
    fecha = models.DateField(auto_now=True)
    monto = models.FloatField()
    def __unicode__(self):
        return self.deudas
