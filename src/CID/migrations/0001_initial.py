# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Articulo'
        db.create_table(u'CID_articulo', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('desc', self.gf('django.db.models.fields.TextField')(null=True)),
            ('costo', self.gf('django.db.models.fields.FloatField')()),
            ('off', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('precio', self.gf('django.db.models.fields.FloatField')()),
        ))
        db.send_create_signal(u'CID', ['Articulo'])

        # Adding model 'Region'
        db.create_table(u'CID_region', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'CID', ['Region'])

        # Adding model 'Provincia'
        db.create_table(u'CID_provincia', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['CID.Region'])),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'CID', ['Provincia'])

        # Adding model 'Comuna'
        db.create_table(u'CID_comuna', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('provincia', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['CID.Provincia'])),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'CID', ['Comuna'])

        # Adding model 'Resinto'
        db.create_table(u'CID_resinto', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('comuna', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['CID.Comuna'])),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('direccion', self.gf('django.db.models.fields.TextField')(null=True)),
            ('telefonos', self.gf('django.db.models.fields.CharField')(max_length=30, null=True)),
            ('observaciones', self.gf('django.db.models.fields.TextField')(null=True)),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal(u'CID', ['Resinto'])

        # Adding model 'Prefijo'
        db.create_table(u'CID_prefijo', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=10)),
        ))
        db.send_create_signal(u'CID', ['Prefijo'])

        # Adding model 'Cliente'
        db.create_table(u'CID_cliente', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('num', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('nombres', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('telefonos', self.gf('django.db.models.fields.CharField')(max_length=30, null=True)),
            ('direccion', self.gf('django.db.models.fields.TextField')(null=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True)),
            ('prefijo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['CID.Prefijo'])),
            ('tipo', self.gf('django.db.models.fields.CharField')(default=2, max_length=1)),
            ('habilitado', self.gf('django.db.models.fields.NullBooleanField')(default=False, null=True, blank=True)),
            ('afiliado', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'CID', ['Cliente'])

        # Adding M2M table for field recintos on 'Cliente'
        m2m_table_name = db.shorten_name(u'CID_cliente_recintos')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('cliente', models.ForeignKey(orm[u'CID.cliente'], null=False)),
            ('resinto', models.ForeignKey(orm[u'CID.resinto'], null=False))
        ))
        db.create_unique(m2m_table_name, ['cliente_id', 'resinto_id'])

        # Adding model 'Deuda'
        db.create_table(u'CID_deuda', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cliente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['CID.Cliente'])),
            ('cuotas_pendientes', self.gf('django.db.models.fields.IntegerField')()),
            ('fecha_inicial', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
            ('prorroga', self.gf('django.db.models.fields.FloatField')(default=1.0)),
            ('total', self.gf('django.db.models.fields.FloatField')()),
            ('estado', self.gf('django.db.models.fields.CharField')(default=0, max_length=1)),
        ))
        db.send_create_signal(u'CID', ['Deuda'])

        # Adding M2M table for field articulos on 'Deuda'
        m2m_table_name = db.shorten_name(u'CID_deuda_articulos')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('deuda', models.ForeignKey(orm[u'CID.deuda'], null=False)),
            ('articulo', models.ForeignKey(orm[u'CID.articulo'], null=False))
        ))
        db.create_unique(m2m_table_name, ['deuda_id', 'articulo_id'])

        # Adding model 'Pago'
        db.create_table(u'CID_pago', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('deudas', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['CID.Deuda'])),
            ('observaciones', self.gf('django.db.models.fields.TextField')(null=True)),
            ('fecha', self.gf('django.db.models.fields.DateField')(auto_now=True, blank=True)),
            ('monto', self.gf('django.db.models.fields.FloatField')()),
        ))
        db.send_create_signal(u'CID', ['Pago'])


    def backwards(self, orm):
        # Deleting model 'Articulo'
        db.delete_table(u'CID_articulo')

        # Deleting model 'Region'
        db.delete_table(u'CID_region')

        # Deleting model 'Provincia'
        db.delete_table(u'CID_provincia')

        # Deleting model 'Comuna'
        db.delete_table(u'CID_comuna')

        # Deleting model 'Resinto'
        db.delete_table(u'CID_resinto')

        # Deleting model 'Prefijo'
        db.delete_table(u'CID_prefijo')

        # Deleting model 'Cliente'
        db.delete_table(u'CID_cliente')

        # Removing M2M table for field recintos on 'Cliente'
        db.delete_table(db.shorten_name(u'CID_cliente_recintos'))

        # Deleting model 'Deuda'
        db.delete_table(u'CID_deuda')

        # Removing M2M table for field articulos on 'Deuda'
        db.delete_table(db.shorten_name(u'CID_deuda_articulos'))

        # Deleting model 'Pago'
        db.delete_table(u'CID_pago')


    models = {
        u'CID.articulo': {
            'Meta': {'object_name': 'Articulo'},
            'costo': ('django.db.models.fields.FloatField', [], {}),
            'desc': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'off': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'precio': ('django.db.models.fields.FloatField', [], {})
        },
        u'CID.cliente': {
            'Meta': {'object_name': 'Cliente'},
            'afiliado': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True'}),
            'habilitado': ('django.db.models.fields.NullBooleanField', [], {'default': 'False', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombres': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'num': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'prefijo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['CID.Prefijo']"}),
            'recintos': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['CID.Resinto']", 'symmetrical': 'False'}),
            'telefonos': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'default': '2', 'max_length': '1'})
        },
        u'CID.comuna': {
            'Meta': {'object_name': 'Comuna'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'provincia': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['CID.Provincia']"})
        },
        u'CID.deuda': {
            'Meta': {'object_name': 'Deuda'},
            'articulos': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['CID.Articulo']", 'symmetrical': 'False'}),
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['CID.Cliente']"}),
            'cuotas_pendientes': ('django.db.models.fields.IntegerField', [], {}),
            'estado': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '1'}),
            'fecha_inicial': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'prorroga': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'total': ('django.db.models.fields.FloatField', [], {})
        },
        u'CID.pago': {
            'Meta': {'object_name': 'Pago'},
            'deudas': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['CID.Deuda']"}),
            'fecha': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'monto': ('django.db.models.fields.FloatField', [], {}),
            'observaciones': ('django.db.models.fields.TextField', [], {'null': 'True'})
        },
        u'CID.prefijo': {
            'Meta': {'object_name': 'Prefijo'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'CID.provincia': {
            'Meta': {'object_name': 'Provincia'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['CID.Region']"})
        },
        u'CID.region': {
            'Meta': {'object_name': 'Region'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'CID.resinto': {
            'Meta': {'object_name': 'Resinto'},
            'comuna': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['CID.Comuna']"}),
            'direccion': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'observaciones': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'telefonos': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        }
    }

    complete_apps = ['CID']