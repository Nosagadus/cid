# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Renaming column for 'Cliente.prefijo' to match new field type.
        db.rename_column(u'CID_cliente', 'prefijo', 'prefijo_id')
        # Changing field 'Cliente.prefijo'
        db.alter_column(u'CID_cliente', 'prefijo_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['CID.Prefijo']))
        # Adding index on 'Cliente', fields ['prefijo']
        db.create_index(u'CID_cliente', ['prefijo_id'])


    def backwards(self, orm):
        # Removing index on 'Cliente', fields ['prefijo']
        db.delete_index(u'CID_cliente', ['prefijo_id'])


        # Renaming column for 'Cliente.prefijo' to match new field type.
        db.rename_column(u'CID_cliente', 'prefijo_id', 'prefijo')
        # Changing field 'Cliente.prefijo'
        db.alter_column(u'CID_cliente', 'prefijo', self.gf('django.db.models.fields.CharField')(max_length=4))

    models = {
        u'CID.articulo': {
            'Meta': {'object_name': 'Articulo'},
            'costo': ('django.db.models.fields.FloatField', [], {}),
            'desc': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'off': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'precio': ('django.db.models.fields.FloatField', [], {})
        },
        u'CID.cliente': {
            'Meta': {'object_name': 'Cliente'},
            'afiliado': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True'}),
            'habilitado': ('django.db.models.fields.NullBooleanField', [], {'default': 'False', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombres': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'num': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'prefijo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['CID.Prefijo']"}),
            'recintos': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['CID.Resinto']", 'symmetrical': 'False'}),
            'telefonos': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'default': '2', 'max_length': '1'})
        },
        u'CID.comuna': {
            'Meta': {'object_name': 'Comuna'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'provincia': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['CID.Provincia']"})
        },
        u'CID.deuda': {
            'Meta': {'object_name': 'Deuda'},
            'articulos': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['CID.Articulo']", 'symmetrical': 'False'}),
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['CID.Cliente']"}),
            'cuotas_pendientes': ('django.db.models.fields.IntegerField', [], {}),
            'estado': ('django.db.models.fields.CharField', [], {'default': '0', 'max_length': '1'}),
            'fecha_inicial': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'prorroga': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'total': ('django.db.models.fields.FloatField', [], {})
        },
        u'CID.pago': {
            'Meta': {'object_name': 'Pago'},
            'deudas': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['CID.Deuda']"}),
            'fecha': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'monto': ('django.db.models.fields.FloatField', [], {}),
            'observaciones': ('django.db.models.fields.TextField', [], {'null': 'True'})
        },
        u'CID.prefijo': {
            'Meta': {'object_name': 'Prefijo'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'CID.provincia': {
            'Meta': {'object_name': 'Provincia'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['CID.Region']"})
        },
        u'CID.region': {
            'Meta': {'object_name': 'Region'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'CID.resinto': {
            'Meta': {'object_name': 'Resinto'},
            'comuna': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['CID.Comuna']"}),
            'direccion': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'observaciones': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'telefonos': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        }
    }

    complete_apps = ['CID']