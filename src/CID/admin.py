from django.contrib import admin
from CID.models import Articulo, Cliente, Resinto, Deuda, Pago, Region, Provincia, Comuna, Prefijo, Exposicion,Venta


# Register your models here.
admin.site.register(Articulo)
admin.site.register(Cliente) 
admin.site.register(Resinto)
admin.site.register(Deuda)
admin.site.register(Pago)
admin.site.register(Region)
admin.site.register(Provincia)
admin.site.register(Comuna)
admin.site.register(Prefijo)
admin.site.register(Exposicion)
admin.site.register(Venta)